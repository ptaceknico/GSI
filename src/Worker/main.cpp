#include <iostream>

using namespace std;

#define MAX_LEN     1024

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//		-----------
//		 Worker
//		-----------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>

#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>

//----------------------------------------------------------------------

struct	    sockaddr_in	as, ac;
struct	    hostent *	h;
short		userport	= 5000;
int			theUnique;
int			rc;
socklen_t	lad		= sizeof(as);
char		work[100];


//----------------------------------------------------------------------

int doMsgProcessing() {

    // Receiving the work to do
	if (recv(theUnique, work, sizeof(work), 0) <0) {
		perror("Error recv :");
	}
	printf("Received : %s \n", work);
	printf	("-----------------------\n");

    // Checking if we got a work to do or not
	if(strlen(work) == 0){
        printf  ("No work to do \n");
        printf	("-----------------------\n");
	} else
	{

        // The "answer" of the work is displayed in the file f to be transfered to the server
        FILE * f = popen(work, "r");

        char pwd[MAX_LEN] = {0};
        fgets(pwd, MAX_LEN, f);

        // Displaying + Sending the result to the server
        while(!feof(f)){
            printf("%s", pwd);

            if(send(theUnique, pwd, sizeof(pwd),0)<0)
            {
                perror("send failed");
            }

            fgets(pwd, MAX_LEN, f);
        }
        pclose(f);

        printf	("-----------------------\n");
        printf  ("Work done \n");
        printf	("-----------------------\n");
    }
}

//----------------------------------------------------------------------

int myGo() {
	h = gethostbyname ("localhost");

	if(! h)
	{
		perror(" Pb gethostbyname: ");
		exit	(-1);
	}

	as.sin_family	= AF_INET;
	as.sin_port	= htons	(userport);
	memcpy	(&as.sin_addr, h->h_addr, 4);

	theUnique = socket(AF_INET, SOCK_STREAM, 0);

	if (theUnique < 0)
	{
		perror(" Pb socket: ");
		exit	(-2);
	}

	rc = connect(theUnique, (struct sockaddr *)&as, lad);

	if(rc < 0)
	{
		perror(" Pb connect: ");
		exit	(-3);
	}
    // Tell the server i'm a worker
	char buffer[32] = "worker";
	if(send(theUnique, buffer, sizeof(buffer),0)<0)
	{
		perror("send failed \n");
	}

	doMsgProcessing();

	close	(theUnique);
}

//----------------------------------------------------------------------

int main(int argc, char * argv[]) {
    system("clear");
	printf	("-----------------------\n");
	printf	(" Worker as a Client: \n");
	printf	("-----------------------\n\n");

	userport = atoi(argv[1]);           // Useport
	sleep	(2);
	myGo();
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------
