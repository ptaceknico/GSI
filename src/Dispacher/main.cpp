#include <iostream>
#include <SFML/System.hpp>
#include <vector>
#include <map>

using namespace std;
using namespace sf;

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//		------------
//		 Dispacher:
//		------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>

#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>

//----------------------------------------------------------------------

struct	    sockaddr_in	as, ac;
struct	    hostent *	h;
short		userport	= 5000;
int			theConnection;
int			theConversation;
int			rc;
socklen_t	lad		= sizeof(as);

char		ligne[100];

map<string, int>    priority_map;
map<string, int>::iterator it;

map<string, string> res_map;
map<string, string>::iterator it2;

char 		name[100];
char        work_tmp[128];
char        work_res[1024];


//----------------------------------------------------------------------

bool compare(pair<string, int> i, pair<string, int> j){
    return i.second < j.second;
}

int calculatePriority (int type, int disk, int power, int cputime) {
	int res = 0;
	switch (type) {
		case 1:			// Maths
		res = (disk+power)*cputime;
		break;

		case 2:			// Disk
		res = disk*(power+cputime);
		break;

		case 3:			// Transactionnel
		res = disk*power*cputime;
		break;

		default:		// Wrong type
		res = disk+power+cputime;
		break;
	}
	return res;
}

int worker() {
    system	("clear");

    // Checking if we have to give to the worker to do
    if(priority_map.size() == 0){
        printf	("-----------------------\n");
        printf	("No work to send to the worker \n");
        printf	("-----------------------\n");
    } else
    {
        // Sending the work with the lowest priority to the worker
        pair<string, int> min = *min_element(priority_map.begin(), priority_map.end(), compare);
        strcpy(name, min.first.c_str());

        priority_map.erase(min.first);

        printf	("-----------------------\n");
        printf	("Sending work to do to worker \n");
        printf	("-----------------------\n");


        if(send(theConversation, name, sizeof(name),0)<0)
        {
            perror("send failed \n");
        }
        printf	("Worker is doing your work (%s) ... \n", name);
        printf	("-----------------------\n");
        printf	("Result : \n");

        // Receiving the result from the server
        memset(work_res, '\0', sizeof(work_res));
        while(recv(theConversation, work_tmp, sizeof(work_tmp), 0) >0) {
                printf(work_tmp);
                strcat(work_res, work_tmp);
        }
        res_map[name] = work_res;

        printf	("-----------------------\n");
        printf	("--> Connect the client again to get the result of the request\n");
        printf	("-----------------------\n");
    }
}

//-----------------------------------------------------------------------

int doMsgProcessing() {

	int type;
	int disk;
	int power;
	int cputime;
	int priority;

	int nb_info;

	// Receiving the number of info to be read
	if (recv(theConversation, ligne, sizeof(ligne), 0) <0) {
		perror("Error recv ");
	}
	nb_info = atoi(ligne);
	printf("Receiving %d infos\n", nb_info);
	printf	("-----------------------\n");

	// Reading the actual file
	while (nb_info >0){
		if (recv(theConversation, ligne, sizeof(ligne), 0) <0) {
			perror("Error recv ");
		}

		int z = strlen(ligne) - 2;

		if(z > 0)
		while (z--)
		{
			if (ligne[z]==' ')	ligne[z]='\0';
			if (ligne[z]=='\t')	ligne[z]='\0';
		}
		printf	("Server uses:  <%s>\n", ligne);

		char temp[100];

        // Saving the content of the file in different variables
		if (! memcmp(ligne, "proc=", strlen("proc=")-1)){
			strcpy(name,&ligne[strlen("proc=")]);
		}

		if (! memcmp(ligne, "profil=", strlen("profil=")-1)){
			type = atoi(strcpy(temp,&ligne[strlen("profil=")]));
		}

		if (! memcmp(ligne, "disk=", strlen("disk=")-1)){
			disk = atoi(strcpy(temp,&ligne[strlen("disk=")]));
		}

		if (! memcmp(ligne, "power=", strlen("power=")-1)){
			power = atoi(strcpy(temp,&ligne[strlen("power=")]));
		}

		if (! memcmp(ligne, "cputime=", strlen("cputime=")-1)){
			cputime = atoi(strcpy(temp,&ligne[strlen("cputime=")]));
		}

		printf("----------------------- \n");
		nb_info--;
	}

    // Calculate the priority of the "workunit"
	priority = calculatePriority(type,disk,power,cputime);

	priority_map[name] = priority;

	char buffer[32];
	sprintf(buffer, "Your priority is %i", priority);

    // Sending the priority to the client for information
	if(send(theConversation, buffer, sizeof(buffer),0)<0)
	{
		perror("send failed \n");
	}

	system	("clear");
	printf	("-----------------------\n");
	printf	("Transmission with EndUser done.\n");
	printf	("Priority is %d \n", priority);
	printf	("-----------------------\n");

    // If the result if store in the map of result, send it to the client
    it2 = res_map.find(name);

    if(it2 != res_map.end()){

        strcpy(work_res, (it2->second).c_str());

        if(send(theConversation, work_res, sizeof(work_res),0)<0)
        {
            perror("send failed \n");
        }
        res_map.erase(it2->first);
        printf	("Result has been sent to the client\n");
        printf	("-----------------------\n");
    }
}

//----------------------------------------------------------------------

int myGo() {
	h = gethostbyname ("localhost");

	if (! h)
	{
		perror(" Pb gethostbyname: ");
		exit	(-1);
	}

	as.sin_family	= AF_INET;
	as.sin_port	= htons	(userport);
	memcpy	(&as.sin_addr, h->h_addr, 4);

	theConnection	= socket(AF_INET, SOCK_STREAM, 0);

	if (theConnection < 0)
	{
		perror(" Pb socket: ");
		exit	(-2);
	}

    // Bind
	rc = bind(theConnection, (struct sockaddr *)&as, lad);

	if (rc < 0)
	{
		perror(" Pb bind: ");
		exit	(-3);
	}

    // Listen
	rc = listen(theConnection, 4);

	if (rc < 0)
        {
                perror(" Pb listen: ");
                exit    (-4);
        }

	while(1)
	{
		// Waiting for new connection
		printf	("\nWaiting on port %d ...\n\n", userport);
		theConversation = accept(theConnection, (struct sockaddr *)&ac, (socklen_t *)&lad);

        if (theConversation < 0)
        {
                perror(" Pb accept: ");
                exit    (-5);
        }

        // Check with who we are dealing : a client or a worker ?
        char buffer[32];
        if (recv(theConversation, buffer, sizeof(buffer), 0) <0) {
            perror("Error recv :");
        }

        printf	("Call received from %s \n\n", buffer);

        // If it's a client do Client processing
        if(strcmp(buffer, "client") == 0){
            Thread thread(&doMsgProcessing);
            thread.launch();

        // If it's a work do worker processing
        } else if(strcmp(buffer, "worker") == 0){
            Thread thread(&worker);
            thread.launch();
        }

        sleep	(3);

        close	(theConversation);

    }
}

//----------------------------------------------------------------------

int main(int argc, char * argv[])
{
    system("clear");
	printf	("------------------------\n");
	printf	(" Dispacher as a Server: \n");
	printf	("------------------------\n\n");

	userport = atoi(argv[1]);                   // Useport
	printf	("---> service/port: %d\n", userport);
	myGo();
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------
