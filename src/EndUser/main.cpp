#include <iostream>

using namespace std;

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//		-----------
//		 End-User:
//		-----------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>

#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>

//----------------------------------------------------------------------

struct	        sockaddr_in	as, ac;
struct	        hostent *	h;
short			userport	= 5000;
int			    theUnique;
int			    rc;
socklen_t		lad		= sizeof(as);

char			fileName[100];
char			ligne[100];
char            work_res[1024];

FILE *			request;

//----------------------------------------------------------------------

int doMsgProcessing() {
    // Opening the "workunit"
	request	= fopen(fileName, "r");
	int tmp = 0;

	if(! request)
	{
		perror(" Pb fopen: ");
		exit	(11);
	}

	// Sending the number of argument to the server
	fgets	(ligne, sizeof(ligne), request);
	if(send(theUnique, ligne, sizeof(ligne),0)<0)
	{
		perror("send failed");
	}

	// Sending the file
	fgets	(ligne, sizeof(ligne), request);

	while	(!feof(request))
	{
		printf	("Client sends: %s", ligne);

		if(send(theUnique, ligne, sizeof(ligne),0)<0)
		{
			perror("send failed");
		}

		fgets	(ligne, sizeof(ligne), request);
	}

	printf	("-----------------------\n");
	printf	("Done.\n");

	// The server notify the client that he received his data
	char buffer[32];

	if (recv(theUnique, buffer, sizeof(buffer), 0) <0) {
		perror("Error recv :");
	}
	printf	("Received : %s \n", buffer);

	printf	("-----------------------\n");

    // Client is trying to get the result of the work
    if(recv(theUnique, work_res, sizeof(work_res), 0) <0) {
        perror("error recv");
    }

    // If nothing is received, launch the program again to get the result
    if(strlen(work_res) == 0){
        printf	("--> Start the program again to get your result\n");
        printf	("-----------------------\n");

    // Else display the result
    } else {
        system  ("clear");
        printf	("-----------------------\n");
        printf  ("Worker has done your work\n");
        printf	("-----------------------\n");

        printf	("Result received from the worker :\n");
        printf  (work_res);
        printf	("-----------------------\n");
    }
//   Loop until receiving a response from the server
//   while(strlen(work_res) == 0){
//        if(recv(theUnique, work_res, sizeof(work_res), 0) <0) {
//            perror("error recv");
//        }
//    }
//    printf	("Result received from the worker :\n");
//    printf(work_res);
//    printf	("-----------------------\n");
}

//----------------------------------------------------------------------

int myGo() {
	h = gethostbyname ("localhost");

	if(! h)
	{
		perror(" Pb gethostbyname: ");
		exit	(-1);
	}

	as.sin_family	= AF_INET;
	as.sin_port	= htons	(userport);
	memcpy	(&as.sin_addr, h->h_addr, 4);

	theUnique = socket(AF_INET, SOCK_STREAM, 0);

	if (theUnique < 0)
	{
		perror(" Pb socket: ");
		exit	(-2);
	}

	rc = connect(theUnique, (struct sockaddr *)&as, lad);

	if(rc < 0)
	{
		perror(" Pb connect: ");
		exit	(-3);
	}
    // Tell the server i'm a client
	char buffer[32]="client";

	if(send(theUnique, buffer, sizeof(buffer),0)<0)
	{
		perror("send failed \n");
	}

	doMsgProcessing();

	close	(theUnique);
}

//----------------------------------------------------------------------

int main(int argc, char * argv[]) {
	system("clear");
	printf	("-----------------------\n");
	printf	(" End-User as a Client: \n");
	printf	("-----------------------\n\n");

	userport = atoi(argv[1]);       // Useport
	strcpy(fileName,argv[2]);       // Name of the workunit to work with
	sleep	(2);
	myGo();
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

