------------------------------------------------------------------------------------------
---------------------------------------README SCHEDULER-----------------------------------
------------------------------------------------------------------------------------------

L'application est composé de 3 programmes :
	- Dispacher
	- EndUser
	- Worker

-> Dispacher
	Source :	src/Dispacher/main.cpp
	Executable :	src/Dispacher/bin/Debug/Dispacher
	To run :	./Dispacher <port>
	Ex :		./Dispacher 5000

-> EndUser
	Source :	src/EndUser/main.cpp
	Executable :	src/EndUser/bin/Debug/EndUser
	To run :	./EndUser <port> <workunit>
	Ex :		./EndUser 5000 workunit1

-> Worker
	Source :	src/Worker/main.cpp
	Executable :	src/Worker/bin/Debug/Worker
	To run :	./Worker <port>
	Ex :		./Worker 5000

------------------------------------------------------------------------------------------
Procédure de test :
------------------------------------------------------------------------------------------

Chaque programme doit être lancé dans un terminal différent

	- Lancer le serveur		./Dispacher 5000
	- Lancer le client		./EndUser 5000 workunit1
	- Lancer le worker		./Worker 5000
	- Relancer le client		./EndUser 5000 workunit1

------------------------------------------------------------------------------------------
